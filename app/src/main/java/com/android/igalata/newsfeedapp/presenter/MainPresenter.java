package com.android.igalata.newsfeedapp.presenter;

import com.android.igalata.newsfeedapp.contract.MainContract;
import com.android.igalata.newsfeedapp.data.NewsDAOImpl;
import com.android.igalata.newsfeedapp.view.fragment.BaseNewsListFragment;
import com.android.igalata.newsfeedapp.view.fragment.BookmarksFragment;
import com.android.igalata.newsfeedapp.view.fragment.NewsFragment;

public class MainPresenter implements MainContract.UserActionsListener {
    private NewsFragment mFirstFragment;
    private BookmarksFragment mSecondFragment;

    @Override
    public BaseNewsListFragment getFragment(int position) {
        if (mFirstFragment == null) {
            mFirstFragment = new NewsFragment();
            mSecondFragment = new BookmarksFragment();
        }
        return position == 0 ? mFirstFragment : mSecondFragment;
    }

    @Override
    public void closeRealm() {
        NewsDAOImpl.getInstance().close();
    }
}
