package com.android.igalata.newsfeedapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;

import com.android.igalata.newsfeedapp.data.News;
import com.android.igalata.newsfeedapp.listener.OnNewsItemClickListener;

import java.util.List;

public abstract class BaseNewsListAdapter extends RecyclerView.Adapter {
    List<News> news;
    Context context;
    OnNewsItemClickListener onNewsItemClickListener;

    public BaseNewsListAdapter(List<News> news, Context context, OnNewsItemClickListener onNewsItemClickListener) {
        this.news = news;
        this.context = context;
        this.onNewsItemClickListener = onNewsItemClickListener;
    }
}
