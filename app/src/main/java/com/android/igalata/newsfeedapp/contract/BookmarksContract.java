package com.android.igalata.newsfeedapp.contract;

import android.support.annotation.StringRes;

import com.android.igalata.newsfeedapp.data.News;

import java.util.List;

public interface BookmarksContract {
    interface View {
        void refresh(List<News> news);

        void showMessage(@StringRes int textRes);
    }

    interface UserActionsListener {
        void refresh();

        void deleteNews(News news);

        void cancelDelete();

        void closeRealm();
    }
}
