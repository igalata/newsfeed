package com.android.igalata.newsfeedapp.contract;

import com.android.igalata.newsfeedapp.data.News;

import java.util.List;

public interface NewsContract {
    interface View {
        void showProgress();

        void hideProgress();

        void refresh(List<News> news);

        void showError(String error);
    }

    interface UserActionsListener {
        void refresh();

        void likeNews(News news);

        void dislikeNews(News news);

        void closeRealm();
    }
}
