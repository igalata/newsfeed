package com.android.igalata.newsfeedapp.listener;

import com.android.igalata.newsfeedapp.data.News;

import java.util.List;

public interface OnNewsLoadedListener {
    void onSuccess(List<News> news);

    void onError(String error);
}
