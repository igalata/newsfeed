package com.android.igalata.newsfeedapp.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.igalata.newsfeedapp.R;
import com.android.igalata.newsfeedapp.adapter.BookmarksAdapter;
import com.android.igalata.newsfeedapp.contract.BookmarksContract;
import com.android.igalata.newsfeedapp.data.News;
import com.android.igalata.newsfeedapp.presenter.BookmarksPresenter;
import com.android.igalata.newsfeedapp.utils.UiUtils;

import java.util.List;

public class BookmarksFragment extends BaseNewsListFragment implements BookmarksContract.View {
    private BookmarksContract.UserActionsListener mListener;
    private BookmarksAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mListener = new BookmarksPresenter(this);
        initList();
        return view;
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_bookmarks;
    }

    @Override
    public void onResume() {
        super.onResume();
        mListener.refresh();
    }

    @Override
    public void refresh(@NonNull List<News> news) {
        if (mAdapter == null) {
            setAdapter(news);
        } else {
            mAdapter.setNews(news);
        }

        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void setAdapter(@NonNull List<News> news) {
        mAdapter = new BookmarksAdapter(getContext(), news, mListener::deleteNews, this::goToWebActivity);
        newsList.setAdapter(mAdapter);
    }

    @Override
    public void showMessage(@StringRes int textRes) {
        UiUtils.showSnackbarWithCancelAction(getView(), textRes, mListener::cancelDelete);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mListener.closeRealm();
    }

    @Override
    public void onPageSelected() {
        if (mListener != null) mListener.refresh();
    }
}
