package com.android.igalata.newsfeedapp.listener;

public interface OnPageSelectedListener {
    void onPageSelected();
}
