package com.android.igalata.newsfeedapp.listener;

import com.android.igalata.newsfeedapp.data.News;

public interface OnNewsItemClickListener {
    void onClick(News news);
}
