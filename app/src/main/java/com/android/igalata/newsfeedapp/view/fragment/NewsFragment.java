package com.android.igalata.newsfeedapp.view.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.igalata.newsfeedapp.R;
import com.android.igalata.newsfeedapp.adapter.NewsAdapter;
import com.android.igalata.newsfeedapp.contract.NewsContract;
import com.android.igalata.newsfeedapp.data.News;
import com.android.igalata.newsfeedapp.presenter.NewsPresenter;
import com.android.igalata.newsfeedapp.utils.UiUtils;

import java.util.List;

import butterknife.Bind;

public class NewsFragment extends BaseNewsListFragment implements NewsContract.View {
    @Bind(R.id.swipe_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private NewsContract.UserActionsListener mListener;
    private NewsAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        mListener = new NewsPresenter(this);
        initList();
        initSwipeRefreshLayout();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mListener.refresh();
    }

    @Override
    protected int getLayoutRes() {
        return R.layout.fragment_news;
    }

    @Override
    public void showProgress() {
        swipeRefreshLayout.post(() -> swipeRefreshLayout.setRefreshing(true));
    }

    @Override
    public void hideProgress() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void refresh(@NonNull List<News> news) {
        if (mAdapter == null) {
            setAdapter(news);
        } else {
            mAdapter.setNews(news);
        }

        mAdapter.notifyDataSetChanged();
    }

    @Override
    protected void setAdapter(@NonNull List<News> news) {
        mAdapter = new NewsAdapter(getContext(), news, mListener::likeNews, mListener::dislikeNews, this::goToWebActivity);
        newsList.setAdapter(mAdapter);
    }

    @Override
    public void showError(String error) {
        UiUtils.showSnackbar(getView(), error);
    }

    private void initSwipeRefreshLayout() {
        swipeRefreshLayout.setColorSchemeResources(R.color.color_primary, R.color.color_accent, R.color.color_primary_dark);
        swipeRefreshLayout.setOnRefreshListener(mListener::refresh);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mListener.closeRealm();
    }

    @Override
    public void onPageSelected() {
        if (mListener != null) mListener.refresh();
    }
}
