package com.android.igalata.newsfeedapp.view.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.android.igalata.newsfeedapp.Const;
import com.android.igalata.newsfeedapp.R;
import com.android.igalata.newsfeedapp.data.News;
import com.android.igalata.newsfeedapp.listener.OnPageSelectedListener;

import java.util.List;

import butterknife.Bind;

public abstract class BaseNewsListFragment extends BaseFragment implements OnPageSelectedListener {
    @Bind(R.id.news_list)
    public RecyclerView newsList;

    void goToWebActivity(@NonNull News news) {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(news.getWebUrl()));
        Bundle extras = new Bundle();
        extras.putBinder(Const.EXTRA_CUSTOM_TABS_SESSION, null);
        intent.putExtra(Const.EXTRA_CUSTOM_TABS_TOOLBAR_COLOR, getContext().getResources().getColor(R.color.color_primary));
        intent.putExtras(extras);
        startActivity(intent);
    }

    void initList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        newsList.setLayoutManager(layoutManager);
    }

    abstract void setAdapter(@NonNull List<News> news);
}
