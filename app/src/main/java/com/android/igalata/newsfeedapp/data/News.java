package com.android.igalata.newsfeedapp.data;

import com.google.gson.annotations.SerializedName;

import io.realm.RealmObject;
import io.realm.annotations.Ignore;
import io.realm.annotations.PrimaryKey;

public class News extends RealmObject {
    @PrimaryKey
    @SerializedName("NewsItemId")
    private String id;
    @SerializedName("HeadLine")
    private String headLine;
    @SerializedName("Keywords")
    private String keywords;
    @SerializedName("WebURL")
    private String webUrl;
    @SerializedName("Image")
    private Image image;
    @SerializedName("DateLine")
    private String date;
    @Ignore
    private boolean isSaved;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHeadLine() {
        return headLine;
    }

    public void setHeadLine(String headLine) {
        this.headLine = headLine;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public boolean isSaved() {
        return isSaved;
    }

    public void setSaved(boolean isSaved) {
        this.isSaved = isSaved;
    }
}
