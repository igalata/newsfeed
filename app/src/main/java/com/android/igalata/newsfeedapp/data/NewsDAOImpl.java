package com.android.igalata.newsfeedapp.data;

import android.support.annotation.NonNull;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

public class NewsDAOImpl implements NewsDAO {
    private static NewsDAOImpl sInstance;
    private static Realm sRealm;

    private NewsDAOImpl() {
    }

    public static NewsDAOImpl getInstance() {
        if (sInstance == null) {
            sInstance = new NewsDAOImpl();
            sRealm = Realm.getDefaultInstance();
        }
        return sInstance;
    }

    public void close() {
        if (sRealm != null) {
            sRealm.close();
            sInstance = null;
            sRealm = null;
        }
    }

    @Override
    public List<News> getNews() {
        return sRealm.allObjects(News.class);
    }

    @Override
    public void deleteNews(@NonNull News news) {
        sRealm.beginTransaction();
        RealmResults<News> results = sRealm.where(News.class)
                .equalTo("id", news.getId()).findAll();
        results.clear();
        sRealm.commitTransaction();
    }

    @Override
    public void addNews(@NonNull News news) {
        sRealm.beginTransaction();
        sRealm.copyToRealmOrUpdate(news);
        sRealm.commitTransaction();
        news.setSaved(true);
    }

    @Override
    public boolean contains(@NonNull News news) {
        return sRealm.where(News.class).equalTo("id", news.getId()).findFirst() != null;
    }
}
