package com.android.igalata.newsfeedapp.data;

import android.support.annotation.NonNull;

import java.util.List;

public interface NewsDAO {
    List<News> getNews();

    void deleteNews(@NonNull News news);

    void addNews(@NonNull News news);

    boolean contains(@NonNull News news);
}
