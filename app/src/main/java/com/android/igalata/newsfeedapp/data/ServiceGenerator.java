package com.android.igalata.newsfeedapp.data;

import com.android.igalata.newsfeedapp.Const;
import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.squareup.okhttp.OkHttpClient;

import io.realm.RealmObject;
import retrofit.GsonConverterFactory;
import retrofit.Retrofit;

public class ServiceGenerator {
    private static ServiceGenerator sInstance;
    private OkHttpClient mClient = new OkHttpClient();
    private Retrofit.Builder mBuilder;
    private Gson mGson;
    private Retrofit mRetrofit;
    private ExclusionStrategy mStrategy = new ExclusionStrategy() {
        @Override
        public boolean shouldSkipField(FieldAttributes f) {
            return f.getDeclaringClass().equals(RealmObject.class);
        }

        @Override
        public boolean shouldSkipClass(Class<?> clazz) {
            return false;
        }
    };

    private ServiceGenerator() {
    }

    public static ServiceGenerator getInstance() {
        if (sInstance == null) sInstance = new ServiceGenerator();
        return sInstance;
    }

    public <S> S createService(Class<S> serviceClass) {
        if (mRetrofit == null) {
            initGson();
            initBuilder();
            mRetrofit = mBuilder.client(mClient).build();
        }

        return mRetrofit.create(serviceClass);
    }

    private void initBuilder() {
        mBuilder = new Retrofit.Builder()
                .baseUrl(Const.ENDPOINT)
                .addConverterFactory(GsonConverterFactory.create(mGson));
    }

    private void initGson() {
        mGson = new GsonBuilder()
                .setExclusionStrategies(mStrategy)
                .create();
    }
}
