package com.android.igalata.newsfeedapp.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.android.igalata.newsfeedapp.R;
import com.android.igalata.newsfeedapp.data.News;
import com.android.igalata.newsfeedapp.listener.OnNewsDislikedListener;
import com.android.igalata.newsfeedapp.listener.OnNewsItemClickListener;
import com.android.igalata.newsfeedapp.listener.OnNewsLikedListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class NewsAdapter extends BaseNewsListAdapter {

    private OnNewsLikedListener mOnNewsLikedListener;
    private OnNewsDislikedListener mOnNewsDislikedListener;

    public NewsAdapter(Context context, List<News> news, OnNewsLikedListener onNewsLikedListener, OnNewsDislikedListener onNewsDislikedListener,
                       OnNewsItemClickListener onNewsItemClickListener) {
        super(news, context, onNewsItemClickListener);
        mOnNewsLikedListener = onNewsLikedListener;
        mOnNewsDislikedListener = onNewsDislikedListener;
    }

    public void setNews(@NonNull List<News> news) {
        this.news = news;
    }

    @Override
    public NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_news, parent, false);
        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        News news = this.news.get(position);
        NewsViewHolder holder = (NewsViewHolder) viewHolder;
        holder.date.setText(news.getDate());
        holder.headLine.setText(news.getHeadLine());
        if (news.getImage() != null) {
            Picasso.with(context).load(news.getImage().getPhoto())
                    .networkPolicy(NetworkPolicy.OFFLINE).into(holder.image, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    Picasso.with(context).load(news.getImage().getPhoto()).into(holder.image);
                }
            });
        }
        holder.buttonLike.setTag(news);
        holder.image.setTag(news);
        holder.buttonLike.setChecked(news.isSaved());
    }

    @Override
    public int getItemCount() {
        return news.size();
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.text_date)
        TextView date;
        @Bind(R.id.text_headline)
        TextView headLine;
        @Bind(R.id.image)
        ImageView image;
        @Bind(R.id.button_like)
        ToggleButton buttonLike;

        public NewsViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnCheckedChanged(R.id.button_like)
        public void onNewsLiked(ToggleButton button) {
            News news = (News) button.getTag();
            if (button.isChecked()) {
                mOnNewsLikedListener.onNewsLiked(news);
            } else {
                mOnNewsDislikedListener.onNewsDisliked(news);
            }
        }

        @OnClick(R.id.image)
        public void onClick(View view) {
            onNewsItemClickListener.onClick((News) view.getTag());
        }
    }
}
