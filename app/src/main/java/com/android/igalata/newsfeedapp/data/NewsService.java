package com.android.igalata.newsfeedapp.data;

import retrofit.Call;
import retrofit.http.GET;

public interface NewsService {
    @GET("/feeds/newsdefaultfeeds.cms?feedtype=sjson")
    Call<NewsResponse> getNews();
}
