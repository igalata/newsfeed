package com.android.igalata.newsfeedapp.view.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.android.igalata.newsfeedapp.R;
import com.android.igalata.newsfeedapp.contract.MainContract;
import com.android.igalata.newsfeedapp.presenter.MainPresenter;
import com.android.igalata.newsfeedapp.view.fragment.BaseNewsListFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    @Bind(R.id.tab_layout)
    TabLayout tabLayout;
    @Bind(R.id.toolbar)
    Toolbar toolbar;
    @Bind(R.id.container)
    ViewPager viewPager;

    private MainContract.UserActionsListener mListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        mListener = new MainPresenter();
        initTabs();
        initViewPager();
        viewPager.setCurrentItem(0);
    }

    private void initTabs() {
        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_title_news));
        tabLayout.addTab(tabLayout.newTab().setText(R.string.tab_title_bookmarks));
        tabLayout.setOnTabSelectedListener(this);
    }

    private void initViewPager() {
        viewPager.setAdapter(new FragmentStatePagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                return mListener.getFragment(position);
            }

            @Override
            public int getCount() {
                return 2;
            }
        });
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (tabLayout != null && tabLayout.getTabAt(position) != null)
                    tabLayout.getTabAt(position).select();
                BaseNewsListFragment fragment = (BaseNewsListFragment) ((FragmentStatePagerAdapter) viewPager.getAdapter()).getItem(position);
                fragment.onPageSelected();
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        viewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {
        // nothing
    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {
        // nothing
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
        mListener.closeRealm();
    }
}
