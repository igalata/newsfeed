package com.android.igalata.newsfeedapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.igalata.newsfeedapp.R;
import com.android.igalata.newsfeedapp.data.News;
import com.android.igalata.newsfeedapp.listener.OnClickDeleteNewsListener;
import com.android.igalata.newsfeedapp.listener.OnNewsItemClickListener;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class BookmarksAdapter extends BaseNewsListAdapter {
    private OnClickDeleteNewsListener mOnClickDeleteListener;

    public BookmarksAdapter(Context context, List<News> news, OnClickDeleteNewsListener listener, OnNewsItemClickListener onClickListener) {
        super(news, context, onClickListener);
        mOnClickDeleteListener = listener;
    }

    public void setNews(List<News> news) {
        this.news = news;
    }

    @Override
    public BookmarkViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_bookmarks, parent, false);
        return new BookmarkViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        BookmarkViewHolder holder = (BookmarkViewHolder) viewHolder;
        News news = this.news.get(position);
        holder.headline.setText(news.getHeadLine());
        holder.date.setText(news.getDate());
        if (news.getImage() != null) {
            Picasso.with(context).load(news.getImage().getPhoto()).networkPolicy(NetworkPolicy.OFFLINE).into(holder.image, new Callback() {
                @Override
                public void onSuccess() {

                }

                @Override
                public void onError() {
                    Picasso.with(context).load(news.getImage().getPhoto()).into(holder.image);
                }
            });
        }
        holder.buttonDelete.setTag(news);
        holder.root.setTag(news);
    }

    @Override
    public int getItemCount() {
        return news.size();
    }

    class BookmarkViewHolder extends RecyclerView.ViewHolder {

        @Bind(R.id.text_headline)
        TextView headline;
        @Bind(R.id.text_date)
        TextView date;
        @Bind(R.id.image)
        ImageView image;
        @Bind(R.id.button_delete)
        ImageButton buttonDelete;
        @Bind((R.id.root))
        View root;

        public BookmarkViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        @OnClick(R.id.button_delete)
        public void onClickDelete(View view) {
            mOnClickDeleteListener.onClickDelete((News) view.getTag());
        }

        @OnClick(R.id.root)
        public void onItemClicked(View view) {
            onNewsItemClickListener.onClick((News) view.getTag());
        }
    }
}
