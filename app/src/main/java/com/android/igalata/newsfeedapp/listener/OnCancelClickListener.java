package com.android.igalata.newsfeedapp.listener;

public interface OnCancelClickListener {
    void onClickCancel();
}
