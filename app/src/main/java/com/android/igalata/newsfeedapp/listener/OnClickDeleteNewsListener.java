package com.android.igalata.newsfeedapp.listener;

import com.android.igalata.newsfeedapp.data.News;

public interface OnClickDeleteNewsListener {
    void onClickDelete(News news);
}
