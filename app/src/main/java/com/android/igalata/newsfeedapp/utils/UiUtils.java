package com.android.igalata.newsfeedapp.utils;

import android.support.design.widget.Snackbar;
import android.view.View;

import com.android.igalata.newsfeedapp.listener.OnCancelClickListener;

public class UiUtils {
    public static void showSnackbar(View view, String text) {
        Snackbar.make(view, text, Snackbar.LENGTH_SHORT).show();
    }

    public static void showSnackbarWithCancelAction(View view, int textRes, OnCancelClickListener listener) {
        Snackbar.make(view, textRes, Snackbar.LENGTH_SHORT).setAction(android.R.string.cancel, v -> {
            listener.onClickCancel();
        }).show();
    }
}
