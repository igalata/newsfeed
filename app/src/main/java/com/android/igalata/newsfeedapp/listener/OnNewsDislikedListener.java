package com.android.igalata.newsfeedapp.listener;

import com.android.igalata.newsfeedapp.data.News;

public interface OnNewsDislikedListener {
    void onNewsDisliked(News news);
}
