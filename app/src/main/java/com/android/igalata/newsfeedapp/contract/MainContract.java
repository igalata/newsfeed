package com.android.igalata.newsfeedapp.contract;

import com.android.igalata.newsfeedapp.view.fragment.BaseFragment;

public interface MainContract {
    interface UserActionsListener {
        BaseFragment getFragment(int position);

        void closeRealm();
    }
}
