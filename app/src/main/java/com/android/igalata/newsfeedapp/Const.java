package com.android.igalata.newsfeedapp;

public class Const {
    public static final String ENDPOINT = "http://timesofindia.indiatimes.com";
    public static final String DB_NAME = "newsfeed";

    //Chrome Custom Tabs extras
    public static final String EXTRA_CUSTOM_TABS_SESSION = "android.support.customtabs.extra.SESSION";
    public static final String EXTRA_CUSTOM_TABS_TOOLBAR_COLOR = "android.support.customtabs.extra.TOOLBAR_COLOR";
}
