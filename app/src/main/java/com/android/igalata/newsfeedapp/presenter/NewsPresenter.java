package com.android.igalata.newsfeedapp.presenter;

import android.support.annotation.NonNull;

import com.android.igalata.newsfeedapp.contract.NewsContract;
import com.android.igalata.newsfeedapp.data.News;
import com.android.igalata.newsfeedapp.data.NewsDAOImpl;
import com.android.igalata.newsfeedapp.data.NewsResponse;
import com.android.igalata.newsfeedapp.data.NewsService;
import com.android.igalata.newsfeedapp.data.ServiceGenerator;
import com.android.igalata.newsfeedapp.listener.OnNewsLoadedListener;

import java.util.List;

import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class NewsPresenter implements NewsContract.UserActionsListener, OnNewsLoadedListener {
    private NewsContract.View mView;

    public NewsPresenter(@NonNull NewsContract.View view) {
        this.mView = view;
    }

    @Override
    public void onSuccess(@NonNull List<News> news) {
        mView.hideProgress();
        mView.refresh(news);
    }

    @Override
    public void onError(@NonNull String error) {
        mView.hideProgress();
        mView.showError(error);
    }

    @Override
    public void closeRealm() {
        NewsDAOImpl.getInstance().close();
    }

    @Override
    public void refresh() {
        mView.showProgress();
        NewsService service = ServiceGenerator.getInstance().createService(NewsService.class);
        Call<NewsResponse> call = service.getNews();
        call.enqueue(new Callback<NewsResponse>() {
            @Override
            public void onResponse(Response<NewsResponse> response, Retrofit retrofit) {
                setSaved(response.body().getNews());
                onSuccess(response.body().getNews());
            }

            @Override
            public void onFailure(Throwable t) {
                onError(t.getMessage());
            }
        });
    }

    private void setSaved(@NonNull List<News> news) {
        for (int i = 0; i < news.size(); ++i) {
            News newsItem = news.get(i);
            newsItem.setSaved(contains(newsItem));
        }
    }

    private boolean contains(@NonNull News news) {
        return NewsDAOImpl.getInstance().contains(news);
    }

    @Override
    public void likeNews(@NonNull News news) {
        NewsDAOImpl.getInstance().addNews(news);
    }

    @Override
    public void dislikeNews(@NonNull News news) {
        NewsDAOImpl.getInstance().deleteNews(news);
    }
}
