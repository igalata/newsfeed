package com.android.igalata.newsfeedapp.presenter;

import android.support.annotation.NonNull;

import com.android.igalata.newsfeedapp.R;
import com.android.igalata.newsfeedapp.contract.BookmarksContract;
import com.android.igalata.newsfeedapp.data.News;
import com.android.igalata.newsfeedapp.data.NewsDAOImpl;

public class BookmarksPresenter implements BookmarksContract.UserActionsListener {
    private BookmarksContract.View mView;
    private News mRemovedNews;

    public BookmarksPresenter(@NonNull BookmarksContract.View view) {
        mView = view;
    }

    @Override
    public void refresh() {
        mView.refresh(NewsDAOImpl.getInstance().getNews());
    }

    @Override
    public void deleteNews(News news) {
        mRemovedNews = copy(news);
        NewsDAOImpl.getInstance().deleteNews(news);
        mView.showMessage(R.string.text_bookmark_removed);
        refresh();
    }

    @Override
    public void cancelDelete() {
        NewsDAOImpl.getInstance().addNews(mRemovedNews);
        refresh();
    }

    @Override
    public void closeRealm() {
        NewsDAOImpl.getInstance().close();
    }

    private News copy(@NonNull News news) {
        News copy = new News();
        copy.setHeadLine(news.getHeadLine());
        copy.setDate(news.getDate());
        copy.setId(news.getId());
        copy.setImage(news.getImage());
        copy.setWebUrl(news.getWebUrl());
        return copy;
    }
}
