package com.android.igalata.newsfeedapp.listener;

import com.android.igalata.newsfeedapp.data.News;

public interface OnNewsLikedListener {
    void onNewsLiked(News news);
}
